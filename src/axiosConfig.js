import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
  params: {
    api_key: '850d9183e60ecbecbc2e89aa36ad4a52'
  }
})

export default instance;
