import React, { useState, useEffect} from 'react';
import axiosConfig from './axiosConfig'
import SearchBar from './components/SearchBar'
import DetailModal from './components/DetailModal'
import Rating from '@material-ui/lab/Rating';
import InfoIcon from '@material-ui/icons/Info';
import {
  Container,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton
} from '@material-ui/core';

const posterBaseURL = 'https://image.tmdb.org/t/p/w342/'

function App() {
  const [movies, setMovies] = useState([])
  const [filteredMovies, setFilteredMovies] = useState(null)
  const [value, setValue] = useState(null)
  const [searchTerm, setSearchTerm] = useState('')
  const [showModal, setShowModal] = useState(false)
  const [currentMovie, setCurrentMovie] = useState({})
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (searchTerm) {
        getSearch(searchTerm)
      } else {
        getDiscover()
      }
    }, searchTerm ? 1500 : 0)

    return () => clearTimeout(delayDebounceFn)
  }, [searchTerm])

  const getDiscover = () => {
    setIsLoading(true)
    axiosConfig.get('discover/movie', {
      params: {
        sort_by: 'popularity.desc',
        include_adult: false
      }
    })
    .then(resp => {
      setIsLoading(false)
      setFilteredMovies(null)
      setValue(null)
      setMovies(resp.data.results)
    })
    .catch(err => console.error(err) || setIsLoading(false))
  }

  const getSearch = (query) => {
    setIsLoading(true)
    axiosConfig.get('search/movie', {
      params: {
        query,
        include_adult: false
      }
    })
    .then(resp => {
      setIsLoading(false)
      setFilteredMovies(null)
      setValue(null)
      setMovies(resp.data.results)
    })
    .catch(err => console.error(err) || setIsLoading(false))
  }

  const filterChange = (newValue) => {
    if (newValue !== null) {
      const ranges = [
        { min: 0, max: 2 },
        { min: 2, max: 4},
        { min: 4, max: 6},
        { min: 6, max: 8},
        { min: 8, max: 10}
      ]
      const { min, max } = ranges[newValue - 1]
      
      const filteredMovies = movies.filter((mov) => {
        if (mov.vote_average >= min && mov.vote_average <= max){
          return mov
        }
        return null
      })
      setFilteredMovies(filteredMovies)
    } else {
      setFilteredMovies(null)
    }
  }

  const getDetails = (id) => {
    setIsLoading(true)
    axiosConfig.get(`movie/${id}`)
    .then(resp => {
      setIsLoading(false)
      setCurrentMovie(resp.data)
      setShowModal(true)
    })
    .catch(err => console.error(err) || setIsLoading(false))
  }

  const getImagePath = (poster_path) => {
    return poster_path ? `${posterBaseURL}${poster_path}`
      : 'http://placehold.it/342x513'
  }

  const data = filteredMovies !== null ? filteredMovies : movies
  return (
    <Container>
      <DetailModal
        showModal={showModal}
        setShowModal={setShowModal}
        currentMovie={currentMovie}
        getImagePath={getImagePath}
        isLoading={isLoading} />
      <h1 style={{
        background: '#171425',
        margin: 0,
        padding: '20px',
        display: 'flex',
        justifyContent: 'center',
        color: 'white',
        fontSize: '40px'
      }}>Welcome to Corner Cinema</h1>
      <div style={{
        background: 'black',
        padding: '20px',
        color: 'white',
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '40px'
      }}>
        <SearchBar searchTerm={searchTerm} onChange={(val) => setSearchTerm(val)}/>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          Filter by rating:&nbsp;
          <Rating
            style={{ background: 'white' }}
            size="large"
            name="rating-controlled"
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
              filterChange(newValue)
            }}
          />
        </div>
      </div>
      {!movies.length && isLoading ? <p style={{ color: 'white' }}>Loading movies...</p> : null}
      {!movies.length && !isLoading && searchTerm ? <p style={{ color: 'white' }}>No results for: {searchTerm}</p> : null}
      {filteredMovies && !filteredMovies.length ? <p style={{ color: 'white' }}>No found movies with the selected rating :(</p> : null}
      <GridList cellHeight={513}>
        {data.map(({ id, title, poster_path, original_language }) => (
          <GridListTile key={id}>
            <img src={getImagePath(poster_path)} alt={title} />
            <GridListTileBar
              title={title}
              subtitle={<span>Original language: {original_language}</span>}
              actionIcon={
                <IconButton style={{ color: 'white' }} aria-label={`info about ${title}`} onClick={() => getDetails(id)}>
                  <InfoIcon />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </Container>
  );
}

export default App;
