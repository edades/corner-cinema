import React from 'react'

const SearchBar = ({ onChange, searchTerm }) => {
  return (
    <div>
      <label>Search: </label>
      <input type="text" value={searchTerm} onChange={(e) => onChange(e.target.value)}/>
    </div>
  )
}

export default SearchBar