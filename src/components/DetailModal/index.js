import React from 'react'
import Modal from '@material-ui/core/Modal';

const DetailModal = ({ showModal, setShowModal, currentMovie, getImagePath, isLoading }) => {
  const { poster_path, title, overview, status, genres = [] } = currentMovie
  if (!currentMovie) return null

  const getGenres = () => {
    return genres.map(({ id, name }, index) =>
      <i key={id}>{name} {(index < genres.length - 1) ? ' - ' : ''}</i>)
  }

  if (isLoading) {
    return <p>Loading details...</p>
  }

  return (
    <Modal
      open={showModal}
      onClose={() => setShowModal(false)}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div style={{
        width: 500,
        height: 300,
        display: 'flex',
        flexDirection: 'columns',
        background: 'white',
        padding: 20,
        margin: '0 auto',
        top: '20%',
        position: 'relative',
        justifyContent: 'space-between',
        alignItems: 'center' }}>
        <img src={getImagePath(poster_path)} alt={title} widht='30%' height='60%'/>
        <div style={{
          alignSelf: 'flex-start',
          width: '70%'
        }}>
          <h2>{title}</h2>
          <p style={{ maxHeight: 120, overflow: 'scroll' }}><i>{overview}</i></p>
          <p>Status: <b>{status}</b></p>
          {genres.length ? <p>Genres: {getGenres()}</p> : null}
        </div>
      </div>
    </Modal>
  )
}

export default DetailModal
